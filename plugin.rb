# name: Iframe Whitelist
# version: 0.0.1
# authors: scossar
# url: https://bitbucket.org/garjun/iframe-whitelist/src

# whitelist raw iframes posted by users
register_asset 'javascripts/iframe-whitelist.js', :server_side